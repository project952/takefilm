package com.sber.takefilm.controller;

import com.sber.takefilm.model.Order;
import com.sber.takefilm.repository.GenericRepository;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/order")
public class OrderController extends GenericController<Order>{

    protected OrderController(GenericRepository<Order> repository) {
        super(repository);
    }
}
