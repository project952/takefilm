package com.sber.takefilm.controller;

import com.sber.takefilm.model.User;
import com.sber.takefilm.repository.GenericRepository;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController extends GenericController<User>{

    protected UserController(GenericRepository<User> repository) {
        super(repository);
    }
}
