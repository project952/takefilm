package com.sber.takefilm.controller;

import com.sber.takefilm.model.Role;
import com.sber.takefilm.repository.RoleRepository;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/role")
public class RoleController {

    private final RoleRepository roleRepository;

    public RoleController(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    @Operation(description = "Get role by id. No request body is required", method = "GetOne")
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(roleRepository.findById(id).orElseThrow());
    }

    @Operation(description = "Create role. Request body is required", method = "Create")
    @PostMapping("/add")
    public ResponseEntity<?> create (@RequestBody Role role) {
        return ResponseEntity.status(HttpStatus.CREATED).body(roleRepository.save(role));
    }

    @Operation(description = "Delete role by id. No request body is required", method = "Delete")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        roleRepository.deleteById(id);
    }

    @Operation(description = "Get all roles. No request body is required", method = "GetAll")
    @RequestMapping(value = "/getAll",method = RequestMethod.GET)
    public ResponseEntity<?> getAll() {
        return ResponseEntity.status(HttpStatus.OK).body(roleRepository.findAll());
    }

}
