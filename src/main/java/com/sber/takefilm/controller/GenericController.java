package com.sber.takefilm.controller;

import com.sber.takefilm.model.GenericModel;
import com.sber.takefilm.repository.GenericRepository;
import io.swagger.v3.oas.annotations.Operation;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public abstract class GenericController<T extends GenericModel> {

    private final GenericRepository<T> repository;

    protected GenericController(GenericRepository<T> repository) {
        this.repository = repository;
    }

    @Operation(description = "Get object by id. No request body is required", method = "GetOne")
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(repository.findById(id).orElseThrow());
    }

    @Operation(description = "Create object. Request body is required", method = "Create")
    @PostMapping("/add")
    public ResponseEntity<?> create (@RequestBody T object) {
        return ResponseEntity.status(HttpStatus.CREATED).body(repository.save(object));
    }

    @Operation(description = "Update object by id. Request body and id are required", method = "Update")
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@RequestBody T object, @PathVariable Long id) {
        object.setId(id);
        return ResponseEntity.status(HttpStatus.OK).body(repository.save(object));
    }

    @Operation(description = "Delete object by id. No request body is required", method = "Delete")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        repository.deleteById(id);
    }

    @Operation(description = "Get all objects. No request body is required", method = "GetAll")
    @RequestMapping(value = "/getAll",method = RequestMethod.GET)
    public ResponseEntity<?> getAll() {
        return ResponseEntity.status(HttpStatus.OK).body(repository.findAll());
    }
}
