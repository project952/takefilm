package com.sber.takefilm.controller;

import com.sber.takefilm.model.Director;
import com.sber.takefilm.repository.GenericRepository;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/director")
public class DirectorController extends GenericController<Director>{

    protected DirectorController(GenericRepository<Director> repository) {
        super(repository);
    }
}
