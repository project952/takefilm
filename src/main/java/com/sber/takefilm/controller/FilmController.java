package com.sber.takefilm.controller;

import com.sber.takefilm.model.Film;
import com.sber.takefilm.repository.GenericRepository;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/film")
public class FilmController extends GenericController<Film>{

    protected FilmController(GenericRepository<Film> repository) {
        super(repository);
    }
}
