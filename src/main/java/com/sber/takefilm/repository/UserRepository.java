package com.sber.takefilm.repository;

import com.sber.takefilm.model.User;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends GenericRepository<User> {

}
