package com.sber.takefilm.repository;

import com.sber.takefilm.model.Director;
import org.springframework.stereotype.Repository;

@Repository
public interface DirectorRepository extends GenericRepository<Director> {

}
