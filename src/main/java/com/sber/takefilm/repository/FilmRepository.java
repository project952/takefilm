package com.sber.takefilm.repository;

import com.sber.takefilm.model.Film;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepository extends GenericRepository<Film> {

}
