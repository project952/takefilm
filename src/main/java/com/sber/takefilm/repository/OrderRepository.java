package com.sber.takefilm.repository;

import com.sber.takefilm.model.Order;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository extends GenericRepository<Order> {

}
