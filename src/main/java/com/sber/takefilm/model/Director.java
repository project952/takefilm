package com.sber.takefilm.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "directors")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@SequenceGenerator(name = "default_generator", sequenceName = "directors_seq", allocationSize = 1)
public class Director extends GenericModel {

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "position")
    private String position;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(
            name = "films_directors",
            joinColumns = @JoinColumn(name = "directors_id"),
            inverseJoinColumns = @JoinColumn(name = "film_id"))
    private Set<Film> films = new HashSet<>();
}
