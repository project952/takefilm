package com.sber.takefilm.model;

public enum Genre {
    ACTION("Экшн"),
    ADVENTURE("Приключения"),
    COMEDY("Комедия"),
    DRAMA("Драма"),
    FANTASY("Фэнтези"),
    HORROR("Ужасы"),
    MUSICALS("Мюзикл"),
    MYSTERY("Мистика"),
    ROMANCE("Мелодрама"),
    SCIENCE_FICTION("Научная фантастика"),
    THRILLER("Триллер"),
    WESTERN("Вестерн");

    private final String genreText;

    Genre (String genreText) {
        this.genreText = genreText;
    }

    public String getGenreText() {
        return this.genreText;
    }
}
