package com.sber.takefilm;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class TakeFilmApplication {

    public static void main(String[] args) {
        SpringApplication.run(TakeFilmApplication.class, args);
        log.info("Swagger-ui run on: http://localhost:8080/swagger-ui/index.html");
    }

}
