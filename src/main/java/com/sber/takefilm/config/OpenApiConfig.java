package com.sber.takefilm.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Contact;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

    @Bean
    public OpenAPI takeFilmProject() {
        return new OpenAPI()
                .info(new Info()
                        .description("Service for films rent or purchase")
                        .title("TakeFilm")
                        .version("v.1.0")
                        .contact(new Contact().name("Anastasia L."))
                );
    }
}
